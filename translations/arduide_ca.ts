<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ca" sourcelanguage="en">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="117"/>
        <source>About %0</source>
        <translation>Quant a %0</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="118"/>
        <source>%0</source>
        <translation>%0</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="119"/>
        <source>&lt;a href=&quot;%0&quot;&gt;%0&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;%0&quot;&gt;%0&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="120"/>
        <source>Authors: %0</source>
        <translation>Autors: %0</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="121"/>
        <source>License: %0</source>
        <translation>Llicència: %0</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="122"/>
        <source>Version: %0</source>
        <translation>Versió: %0</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="123"/>
        <source>Internationalization : &lt;a href=&quot;%0&quot;&gt;You can help us translate on transifex.com&lt;/a&gt;</source>
        <translation>Internacionalització : &lt;a href=&quot;%0&quot;&gt;Ens podeu ajudar a traduir a transifex.com&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="124"/>
        <source>&lt;hr /&gt;</source>
        <translation>&lt;hr /&gt;</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="125"/>
        <source>&lt;p&gt;%0 is a Qt-based IDE for the open-source &lt;a href=&quot;http://arduino.cc&quot;&gt;Arduino&lt;/a&gt; electronics prototyping platform.&lt;/p&gt;

&lt;div&gt;This project is an attempt to provide an alternative to the original Java IDE.&lt;/div&gt;
&lt;div&gt;It is faster and provides a richer feature set for experienced developers.&lt;/div&gt;</source>
        <translation>&lt;p&gt;%0 és un entorn de desenvolupament integrat (IDE) basat en Qt pel codi obert &lt;a href=&quot;http://arduino.cc&quot;&gt;Arduino&lt;/a&gt; plataforma de prototipatge electrònica.&lt;/p&gt;

&lt;div&gt;Aquest projecte és un intent de proporcionar una alternativa al IDE Java original.&lt;/div&gt;
&lt;div&gt;Ès més ràpid i proporciona un conjunt de característiques millor pels desenvolupadors experimentats.&lt;/div&gt;</translation>
    </message>
</context>
<context>
    <name>BoardChooser</name>
    <message>
        <location filename="../gui/BoardChooser.cpp" line="16"/>
        <source>Board</source>
        <translation>Placa</translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <location filename="../gui/Browser.cpp" line="145"/>
        <location filename="../gui/Browser.cpp" line="159"/>
        <source>Load error</source>
        <translation>Error de càrrega</translation>
    </message>
    <message>
        <location filename="../gui/Browser.cpp" line="145"/>
        <location filename="../gui/Browser.cpp" line="159"/>
        <source>The selected example could not be opened.</source>
        <translation>L&apos;exemple seleccionat no es pot obrir.</translation>
    </message>
    <message>
        <location filename="../gui/Browser.cpp" line="277"/>
        <source>No documentation found for %1.</source>
        <translation>No es troba la documentació per %1.</translation>
    </message>
</context>
<context>
    <name>Builder</name>
    <message>
        <location filename="../env/Builder.cpp" line="42"/>
        <source>Cannot read file &apos;%1&apos;.</source>
        <translation>No es pot llegir el fitxer &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="70"/>
        <location filename="../env/Builder.cpp" line="98"/>
        <source>Failed to create build directory.</source>
        <translation>No es pot crear el directori build.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="127"/>
        <source>No board selected.</source>
        <translation>Cap Placa està seleccionada.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="133"/>
        <source>No device selected.</source>
        <translation>Cap dispositiu està seleccionat.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="137"/>
        <source>Compiling for %0...</source>
        <translation>Compilant per %0...</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="161"/>
        <location filename="../env/Builder.cpp" line="183"/>
        <location filename="../env/Builder.cpp" line="217"/>
        <source>Compilation failed.</source>
        <translation>La compilació ha fallat.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="168"/>
        <source>Archiving failed.</source>
        <translation>Ha fallat l&apos;arxivat.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="192"/>
        <source>Can&apos;t write the sketch to disk.</source>
        <translation>No puc escriure l&apos;sketch al disc.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="204"/>
        <source>Can&apos;t open main.cxx.</source>
        <translation>No puc obrir main.cxx.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="222"/>
        <source>Linking...</source>
        <translation>Enllaçant...</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="226"/>
        <source>Link failed.</source>
        <translation>L&apos;enllaç ha fallat.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="231"/>
        <source>Sizing...</source>
        <translation>Grandària...</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="234"/>
        <source>Sizing failed.</source>
        <translation>La grandària ha fallat.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="240"/>
        <source>Failed to extract EEPROM.</source>
        <translation>L&apos;extracció de l&apos;EEPROM ha fallat.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="248"/>
        <source>Failed to extract HEX.</source>
        <translation>L&apos;extracció del HEX ha fallat.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="254"/>
        <location filename="../env/Builder.cpp" line="266"/>
        <source>Success.</source>
        <translation>Èxit.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="259"/>
        <source>Uploading to %0...</source>
        <translation>Carregant a %0...</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="262"/>
        <source>Uploading failed.</source>
        <translation>Ha fallat la càrrega.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="306"/>
        <source>Unknown source type: %0</source>
        <translation>Tipus de font desconegut: %0</translation>
    </message>
</context>
<context>
    <name>ConfigBuild</name>
    <message>
        <location filename="../build/ui_ConfigBuild.h" line="56"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigBuild.h" line="57"/>
        <source>Filter devices list</source>
        <translation>Filtre llista de dispositius</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigBuild.h" line="58"/>
        <source>Verbose output</source>
        <translation>Sortida amb comentaris</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="267"/>
        <source>Configuration</source>
        <translation>Configuració</translation>
    </message>
</context>
<context>
    <name>ConfigEditor</name>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="200"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="201"/>
        <source>Font:</source>
        <translation>Font:</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="203"/>
        <source>Choose</source>
        <translation>Escull</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="204"/>
        <source>Color:</source>
        <translation>Color:</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="205"/>
        <source>Selection</source>
        <translation>Selecció</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="207"/>
        <source>Cursor</source>
        <translation>Cursor</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="209"/>
        <source>Foreground</source>
        <translation>Primer pla</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="211"/>
        <source>Background</source>
        <translation>Fons</translation>
    </message>
</context>
<context>
    <name>ConfigPaths</name>
    <message>
        <location filename="../build/ui_ConfigPaths.h" line="95"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigPaths.h" line="96"/>
        <source>Arduino path:</source>
        <translation>Camí Arduino:</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigPaths.h" line="97"/>
        <location filename="../build/ui_ConfigPaths.h" line="99"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigPaths.h" line="98"/>
        <source>Sketchbook path:</source>
        <translation>Camí Sketchbook:</translation>
    </message>
</context>
<context>
    <name>ConfigWidget</name>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="94"/>
        <source>Default</source>
        <translation>Default</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="95"/>
        <source>Comment</source>
        <translation>Comment</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="96"/>
        <source>CommentLine</source>
        <translation>CommentLine</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="97"/>
        <source>CommentDoc</source>
        <translation>CommentDoc</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="98"/>
        <source>Number</source>
        <translation>Number</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="99"/>
        <source>Keyword</source>
        <translation>Keyword</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="100"/>
        <source>DoubleQuotedString</source>
        <translation>DoubleQuotedString</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="101"/>
        <source>SingleQuotedString</source>
        <translation>SingleQuotedString</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="102"/>
        <source>UUID</source>
        <translation>UUID</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="103"/>
        <source>PreProcessor</source>
        <translation>PreProcessor</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="104"/>
        <source>Operator</source>
        <translation>Operator</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="105"/>
        <source>Identifier</source>
        <translation>Identifier</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="106"/>
        <source>UnclosedString</source>
        <translation>UnclosedString</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="107"/>
        <source>VerbatimString</source>
        <translation>VerbatimString</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="108"/>
        <source>Regex</source>
        <translation>Regex</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="109"/>
        <source>CommentLineDoc</source>
        <translation>CommentLineDoc</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="110"/>
        <source>KeywordSet2</source>
        <translation>KeywordSet2</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="111"/>
        <source>CommentDocKeyword</source>
        <translation>CommentDocKeyword</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="112"/>
        <source>CommentDocKeywordError</source>
        <translation>CommentDocKeywordError</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="113"/>
        <source>GlobalClass</source>
        <translation>GlobalClass</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="114"/>
        <source>Editor</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="118"/>
        <source>Paths</source>
        <translation>Paths</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="122"/>
        <source>Build</source>
        <translation>Construcció</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="206"/>
        <source>Choose Arduino path</source>
        <translation>Escull el camí Arduino</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="213"/>
        <source>Choose Sketchbook path</source>
        <translation>Escull el camí Sketchbook</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="235"/>
        <source>Invalid arduino path</source>
        <translation>Invalid arduino path</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="235"/>
        <source>This path does not contain a valid Arduino installation, please choose another.</source>
        <translation>Aquest camí no conté una instal·lació vàlida d&apos;Arduino, si us plau escull-ne un altre.</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="239"/>
        <source>Please restart the ArduIDE</source>
        <translation>Si us plau reinicia ArduIDE</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="239"/>
        <source>Please restart the ArduIDE to account for the Arduino SDK path change.</source>
        <translation>Si us plau reinicia ArduIDE per tenir en compte el canvi de camí de l&apos;Arduino SDK.</translation>
    </message>
</context>
<context>
    <name>DeviceChooser</name>
    <message>
        <location filename="../gui/DeviceChooser.cpp" line="16"/>
        <source>Device</source>
        <translation>Dispositiu</translation>
    </message>
</context>
<context>
    <name>Editor</name>
    <message>
        <location filename="../gui/Editor.cpp" line="30"/>
        <source>Save project</source>
        <translation>Desa projecte</translation>
    </message>
    <message>
        <location filename="../gui/Editor.cpp" line="30"/>
        <source>Directories (*)</source>
        <translation>Directoris (*)</translation>
    </message>
    <message>
        <location filename="../gui/Editor.cpp" line="36"/>
        <location filename="../gui/Editor.cpp" line="46"/>
        <source>Save error</source>
        <translation>Error desant</translation>
    </message>
    <message>
        <location filename="../gui/Editor.cpp" line="36"/>
        <source>The specified location could not be opened for writing.</source>
        <translation>El lloc especificat no es pot obrir per escriure.</translation>
    </message>
    <message>
        <location filename="../gui/Editor.cpp" line="46"/>
        <source>The file could not be opened for writing.</source>
        <translation>El fitxer no es pot obrir per escriure.</translation>
    </message>
</context>
<context>
    <name>FirstTimeWizard</name>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="310"/>
        <source>Wizard</source>
        <translation>Assistent</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="311"/>
        <source>First-time configuration</source>
        <translation>Configuració per primera vegada</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="312"/>
        <source>The Arduino IDE needs some information in order to work correctly.&lt;br /&gt;Please fill them in the form below:</source>
        <translation>L&apos; Arduino IDE necessita una mica d&apos;informació per treballar correctament.&lt;br /&gt;Omple si us plau el formulari següent:</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="313"/>
        <source>I need to locate your Arduino installation to continue.</source>
        <translation>He de localitzar una instal·lació d&apos;Arduino per poder continuar.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="314"/>
        <source>Existing installation (Arduino SDK 0023, 1.0 or 1.0.1)</source>
        <translation>Instal·lació existent (Arduino SDK 0023, 1.0 or 1.0.1)</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="315"/>
        <source>Arduino path:</source>
        <translation>Camí Arduino:</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="316"/>
        <location filename="../build/ui_FirstTimeWizard.h" line="322"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="317"/>
        <source>Automatic installation (Arduino SDK 1.0.1)</source>
        <translation>Instal·lació automàtica (Arduino SDK 1.0.1)</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="318"/>
        <source>&lt;img src=&quot;:/images/16x16/help-hint.png&quot; /&gt;This will download and install the software automatically from &lt;a href=&quot;http://arduino.cc/&quot;&gt;the Arduino website&lt;/a&gt;.</source>
        <translation>&lt;img src=&quot;:/images/16x16/help-hint.png&quot; /&gt;Això descarregarà i instal·larà el programari automàticament des de &lt;a href=&quot;http://arduino.cc/&quot;&gt;la web d&apos;Arduino&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="319"/>
        <location filename="../build/ui_FirstTimeWizard.h" line="328"/>
        <source>&lt;hr /&gt;</source>
        <translation>&lt;hr /&gt;</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="320"/>
        <source>The sketchbook folder is where the IDE will look for existing projects.&lt;br /&gt;It will be created if it does not already exist.</source>
        <translation>La carpeta sketchbook és on l&apos;IDE cerca els projectes existents.&lt;br /&gt;Serà creada si encara no existeix.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="321"/>
        <source>Sketchbook path:</source>
        <translation>Camí Sketchbook:</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="323"/>
        <source>Automatic Installation</source>
        <translation>Instal·lació automàtica</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="324"/>
        <source>The Arduino IDE will now download and install the required software.</source>
        <translation>L&apos; Arduino IDE descarregarà i instal·larà el programari necesari.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="325"/>
        <source>Step 1: Download</source>
        <translation>Pas 1: Descàrrega</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="326"/>
        <location filename="../build/ui_FirstTimeWizard.h" line="330"/>
        <source>TextLabel</source>
        <translation>EtiquetaText</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="327"/>
        <source>Arduino %0 for %1</source>
        <translation>Arduino %0 per %1</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="329"/>
        <source>Step 2: Installation</source>
        <translation>Pas 2: Instal·lació</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="331"/>
        <source>Configuration successful</source>
        <translation>Configuració correcte</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="332"/>
        <source>The Arduino IDE is now ready.</source>
        <translation>L&apos; Arduino IDE està a punt.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="333"/>
        <source>The configuration is complete, you may now start using the IDE.&lt;br /&gt;Thank you for using the software. Enjoy!</source>
        <translation>La configuració s&apos;ha completat, ara pots començar a utilitzar l&apos;IDE.&lt;br /&gt;Gràcies per utilitzar el programari. Disfruta&apos;l!</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="334"/>
        <source>%0 is an unofficial project by %1.</source>
        <translation>%0 és un projecte no oficial de %1.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="335"/>
        <source>More information at: &lt;a href=&quot;%0&quot;&gt;%0&lt;/a&gt;</source>
        <translation>Més informació a: &lt;a href=&quot;%0&quot;&gt;%0&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="139"/>
        <source>Applications (*.app)</source>
        <translation>Aplicacions (*.app)</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="183"/>
        <location filename="../gui/FirstTimeWizard.cpp" line="194"/>
        <source>Invalid path</source>
        <translation>Camí incorrecte</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="183"/>
        <source>Please enter a valid path to your Arduino installation.</source>
        <translation>Introdueix si us plau un camí vàlid de la instal·lació d&apos;Arduino.</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="194"/>
        <source>The sketchbook path could not be created. Please choose another.</source>
        <translation>El camí sketchbook no pot ser creat. Si us plau escullen un altre.</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="221"/>
        <source>Download error</source>
        <translation>Error de descàrrega</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="222"/>
        <source>An error occured during the download:</source>
        <translation>Hi ha hagut un error durant la descàrrega:</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="282"/>
        <location filename="../gui/FirstTimeWizard.cpp" line="296"/>
        <source>Installation error</source>
        <translation>Error d&apos;instal·lació</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="283"/>
        <source>An error occured during the extraction.</source>
        <translation>Hi ha hagut un error durant l&apos;extracció.</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="297"/>
        <source>The extracted Arduino package is not valid.</source>
        <translation>El paquet Arduino extret no és vàlid.</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="306"/>
        <source>Installation</source>
        <translation>Instal·lació</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="307"/>
        <source>Arduino was successfully installed to:</source>
        <translation>Arduino s&apos;ha instal·lat correctament a:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../build/ui_MainWindow.h" line="450"/>
        <source>&amp;Quit</source>
        <translation>&amp;Surt</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="451"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="452"/>
        <source>&amp;New</source>
        <translation>&amp;Nou</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="453"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="454"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copia</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="455"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="456"/>
        <source>&amp;Paste</source>
        <translation>&amp;Enganxa</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="457"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="458"/>
        <source>&amp;Cut</source>
        <translation>&amp;Talla</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="459"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="460"/>
        <source>&amp;Save</source>
        <translation>&amp;Desa</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="461"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="462"/>
        <source>&amp;Open</source>
        <translation>&amp;Obre</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="463"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="464"/>
        <source>&amp;Close</source>
        <translation>&amp;Tanca</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="465"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="466"/>
        <source>&amp;Build (verify)</source>
        <translation>&amp;Construeix (verifica)</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="467"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="468"/>
        <source>&amp;Utilities</source>
        <translation>&amp;Utilitats</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="470"/>
        <source>Utilities</source>
        <translation>Utilitats</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="472"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="473"/>
        <source>&amp;Upload</source>
        <translation>&amp;Carrega</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="474"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="475"/>
        <source>Go to the next tab</source>
        <translation>Vés a la pestanya següent</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="476"/>
        <source>Ctrl+PgDown</source>
        <translation>Ctrl+PgDown</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="477"/>
        <source>Go to the previous tab</source>
        <translation>Vés a la pestanya.prèvia</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="478"/>
        <source>Ctrl+PgUp</source>
        <translation>Ctrl+PgUp</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="479"/>
        <source>&amp;Configure the IDE</source>
        <translation>&amp;Configura el IDE</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="480"/>
        <source>&amp;About %0</source>
        <translation>&amp;Quant a %0</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="481"/>
        <source>About &amp;Qt</source>
        <translation>Quant a &amp;Qt</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="482"/>
        <source>Undo</source>
        <translation>Desfés</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="483"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="484"/>
        <source>Redo</source>
        <translation>Refés</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="485"/>
        <source>Ctrl+Shift+Z</source>
        <translation>Ctrl+Shift+Z</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="486"/>
        <location filename="../build/ui_MainWindow.h" line="488"/>
        <source>Previous</source>
        <translation>Previ</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="490"/>
        <location filename="../build/ui_MainWindow.h" line="492"/>
        <source>Next</source>
        <translation>Següent</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="494"/>
        <source>Contextual help</source>
        <translation>Ajuda contextual</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="495"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="496"/>
        <source>The official arduino website</source>
        <translation>El llloc web oficial</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="497"/>
        <source>The arduino forums</source>
        <translation>Els fòrums arduino</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="498"/>
        <source>Upload to pastebin</source>
        <translation>Carrega a pastebin</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="499"/>
        <location filename="../build/ui_MainWindow.h" line="514"/>
        <source>Find/Replace</source>
        <translation>Cerca/Substitueix</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="501"/>
        <source>Refresh</source>
        <translation>Refresca</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="508"/>
        <source>Libraries</source>
        <translation>Biblioteques</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="515"/>
        <location filename="../build/ui_MainWindow.h" line="517"/>
        <source>Find</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="500"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="502"/>
        <source>&amp;File</source>
        <translation>&amp;Fitxer</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="503"/>
        <source>&amp;Edit</source>
        <translation>&amp;Edita</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="504"/>
        <source>&amp;Settings</source>
        <translation>&amp;Arranjament</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="505"/>
        <source>&amp;Help</source>
        <translation>&amp;Ajuda</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="506"/>
        <source>Community</source>
        <translation>Comunitat</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="507"/>
        <source>&amp;View</source>
        <translation>&amp;Visualitza</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="509"/>
        <source>Edition</source>
        <translation>Edició</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="510"/>
        <source>Hardware</source>
        <translation>Maquinari</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="511"/>
        <source>Build tools</source>
        <translation>Eines de construcció</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="512"/>
        <source>Utility dock</source>
        <translation>Panell d&apos;utilitats</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="513"/>
        <source>Output</source>
        <translation>Sortida</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="516"/>
        <location filename="../build/ui_MainWindow.h" line="518"/>
        <source>Replace</source>
        <translation>Substitueix</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="519"/>
        <source>Replace All</source>
        <translation>Substitueix-ho tot</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="520"/>
        <source>Reg. Exp.</source>
        <translation>Reg. Exp.</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="521"/>
        <source>Word only</source>
        <translation>Només paraula</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="522"/>
        <source>Case sensitive</source>
        <translation>Distingeix majúscules/minúscules</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="135"/>
        <source>Browser</source>
        <translation>Navegador</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="164"/>
        <source>Device</source>
        <translation>Dispositiu</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="181"/>
        <source>Board</source>
        <translation>Placa</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="247"/>
        <source>Close project</source>
        <translation>Tanca projecte</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="248"/>
        <source>This project has unsaved changes.&lt;br /&gt;Are you sure you want to close it?</source>
        <translation>Aquest projecte té canvis no desats.&lt;br /&gt;Segur que vols tancar-lo?</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="360"/>
        <location filename="../gui/MainWindow.cpp" line="380"/>
        <source>Arduino Libraries</source>
        <translation>Biblioteques Arduino</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="370"/>
        <source>ArduIDE Libraries</source>
        <translation>Biblioteques ArduIDE</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="389"/>
        <source>Install new libraries?</source>
        <translation>Instal·la biblioteques noves?</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="426"/>
        <source>Pastebin error</source>
        <translation>Pastebin error</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="426"/>
        <source>The pastebin upload failed with code:
%1</source>
        <translation>La càrrega pastebin ha fallat amb el codi:
%1</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="466"/>
        <source>Arduide - No occurence found</source>
        <translation>Arduide - No s&apos;ha trobat l&apos;ocurrència</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="467"/>
        <source>No occurence of &apos;%1&apos; found</source>
        <translation>No s&apos;ha trobat l&apos;ocurrència &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="503"/>
        <source>Arduide - Replace All</source>
        <translation>Arduide - Substitueix-ho tot</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="504"/>
        <source>The Replace All feature replaced %1 occurences</source>
        <translation>Substitueix-ho tot ha substituit %1 ocurrències</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="540"/>
        <source>To install a new library, just extract it in this directory.

More information at http://arduino.cc/en/Guide/Environment#libraries</source>
        <translation>Per instal·lar una nova biblioteca només l&apos;has estreure en aquest directori.

Més informació a http://arduino.cc/en/Guide/Environment#libraries</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="554"/>
        <source>Open project</source>
        <translation>Obrir projecte</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="554"/>
        <source>Arduino sketches (*.ino *.pde)</source>
        <translation>Arduino sketches (*.ino *.pde)</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="562"/>
        <source>Open error</source>
        <translation>Error obrint</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="562"/>
        <source>The file could not be opened for reading.</source>
        <translation>Aquet fitxer no pot ser obert per llegir.</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="677"/>
        <source>Quit</source>
        <translation>Surt</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="678"/>
        <source>Some projects in your workspace have unsaved changes.&lt;br /&gt;Are you sure you want to quit?</source>
        <translation>Alguns projectes en el teu espai de treball tenen canvis no desats.&lt;br /&gt;Segur que vols sortir?</translation>
    </message>
</context>
<context>
    <name>OutputView</name>
    <message>
        <location filename="../gui/OutputView.cpp" line="45"/>
        <source>&gt;&gt;&gt;&gt; %0</source>
        <translation>&gt;&gt;&gt;&gt; %0</translation>
    </message>
</context>
<context>
    <name>QHexView</name>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="144"/>
        <source>Set &amp;Font</source>
        <translation>Configura &amp;Font</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="146"/>
        <source>Show A&amp;ddress</source>
        <translation>Mostra A&amp;ddreça</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="147"/>
        <source>Show &amp;Hex</source>
        <translation>Mostra &amp;Hex</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="148"/>
        <source>Show &amp;Ascii</source>
        <translation>Mostra &amp;Ascii</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="153"/>
        <source>Set Word Width</source>
        <translation>Configura l&apos;ample de la paraula</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="154"/>
        <source>1 Byte</source>
        <translation>1 Byte</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="155"/>
        <source>2 Bytes</source>
        <translation>2 Bytes</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="156"/>
        <source>4 Bytes</source>
        <translation>4 Bytes</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="157"/>
        <source>8 Bytes</source>
        <translation>8 Bytes</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="168"/>
        <source>Set Row Width</source>
        <translation>Configura l&apos;ample de la fila</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="169"/>
        <source>1 Word</source>
        <translation>1 Word</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="170"/>
        <source>2 Words</source>
        <translation>2 Words</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="171"/>
        <source>4 Words</source>
        <translation>4 Words</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="172"/>
        <source>8 Words</source>
        <translation>8 Words</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="173"/>
        <source>16 Words</source>
        <translation>16 Words</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="188"/>
        <source>&amp;Copy Selection To Clipboard</source>
        <translation>&amp;Copia Selecció al Porta-retalls</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../env/Device.cpp" line="97"/>
        <source>Serial callout</source>
        <translation>Trucada sortint Serial</translation>
    </message>
    <message>
        <location filename="../env/Device.cpp" line="111"/>
        <source>Serial dialin</source>
        <translation>Trucada entrant Serial</translation>
    </message>
</context>
<context>
    <name>Serial</name>
    <message>
        <location filename="../utils/unix/Serial.cpp" line="23"/>
        <source>Device (%0) already open</source>
        <translation>El dispositiu (%0) ja està obert</translation>
    </message>
    <message>
        <location filename="../utils/unix/Serial.cpp" line="40"/>
        <source>Unknown baud rate %0</source>
        <translation>Desconegut baud rate %0</translation>
    </message>
</context>
</TS>
