<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="117"/>
        <source>About %0</source>
        <translation>Über %0</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="118"/>
        <source>%0</source>
        <translation>%0</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="119"/>
        <source>&lt;a href=&quot;%0&quot;&gt;%0&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;%0&quot;&gt;%0&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="120"/>
        <source>Authors: %0</source>
        <translation>Author: %0</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="121"/>
        <source>License: %0</source>
        <translation>Lizenz: %0</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="122"/>
        <source>Version: %0</source>
        <translation>Version: %0</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="123"/>
        <source>Internationalization : &lt;a href=&quot;%0&quot;&gt;You can help us translate on transifex.com&lt;/a&gt;</source>
        <translation>Internationalisation : &lt;a href=&quot;%0&quot;&gt;Sie können uns helfen beim Übersetzen auf transifex.com&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="124"/>
        <source>&lt;hr /&gt;</source>
        <translation>&lt;hr /&gt;</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="125"/>
        <source>&lt;p&gt;%0 is a Qt-based IDE for the open-source &lt;a href=&quot;http://arduino.cc&quot;&gt;Arduino&lt;/a&gt; electronics prototyping platform.&lt;/p&gt;

&lt;div&gt;This project is an attempt to provide an alternative to the original Java IDE.&lt;/div&gt;
&lt;div&gt;It is faster and provides a richer feature set for experienced developers.&lt;/div&gt;</source>
        <translation type="unfinished">&lt;p&gt;%0 ist eine  Qt-basierende IDE für die open-source &lt;a href=&quot;http://arduino.cc&quot;&gt;Arduino&lt;/a&gt; elektronische Prototyping Platform.&lt;/p&gt;
&lt;div&gt;Dieses Projekt ein Versuch eine Alternative zur Original Java IDE.&lt;/div&gt;
&lt;div&gt;es ist schneller und hat mehr Features für versierte Entwicklier.&lt;/div&gt;</translation>
    </message>
</context>
<context>
    <name>BoardChooser</name>
    <message>
        <location filename="../gui/BoardChooser.cpp" line="16"/>
        <source>Board</source>
        <translation>Print</translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <location filename="../gui/Browser.cpp" line="145"/>
        <location filename="../gui/Browser.cpp" line="159"/>
        <source>Load error</source>
        <translation>Fehler beim Laden</translation>
    </message>
    <message>
        <location filename="../gui/Browser.cpp" line="145"/>
        <location filename="../gui/Browser.cpp" line="159"/>
        <source>The selected example could not be opened.</source>
        <translation>Beispiel konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../gui/Browser.cpp" line="277"/>
        <source>No documentation found for %1.</source>
        <translation>Hilfe nicht gefunden für %1.</translation>
    </message>
</context>
<context>
    <name>Builder</name>
    <message>
        <location filename="../env/Builder.cpp" line="42"/>
        <source>Cannot read file &apos;%1&apos;.</source>
        <translation>Lesen der Datei &apos;%1&apos; nicht möglich.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="70"/>
        <location filename="../env/Builder.cpp" line="98"/>
        <source>Failed to create build directory.</source>
        <translation>Fehler beim erstellen des build Verzeichnis.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="127"/>
        <source>No board selected.</source>
        <translation>Keinen Print selektiert.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="133"/>
        <source>No device selected.</source>
        <translation>Keine Schnittstelle definiert.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="137"/>
        <source>Compiling for %0...</source>
        <translation>Kompilliere  %0...</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="161"/>
        <location filename="../env/Builder.cpp" line="183"/>
        <location filename="../env/Builder.cpp" line="217"/>
        <source>Compilation failed.</source>
        <translation>Fehler beim Kompillieren.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="168"/>
        <source>Archiving failed.</source>
        <translation>Archivierung fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="192"/>
        <source>Can&apos;t write the sketch to disk.</source>
        <translation>Schreiben des sketch auf Disk nicht möglich.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="204"/>
        <source>Can&apos;t open main.cxx.</source>
        <translation>Fehler beim öffnen der Datei main.cxx.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="222"/>
        <source>Linking...</source>
        <translation>Binden...</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="226"/>
        <source>Link failed.</source>
        <translation>Die Bindung hat fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="231"/>
        <source>Sizing...</source>
        <translation>Dimensionierung...</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="234"/>
        <source>Sizing failed.</source>
        <translation>Dimensionierung fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="240"/>
        <source>Failed to extract EEPROM.</source>
        <translation>Fehler beim auslesen des EEPROM.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="248"/>
        <source>Failed to extract HEX.</source>
        <translation>Fehler beim auslesen vom HEX.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="254"/>
        <location filename="../env/Builder.cpp" line="266"/>
        <source>Success.</source>
        <translation>Erfolgreich.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="259"/>
        <source>Uploading to %0...</source>
        <translation>Hochladen auf %0...</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="262"/>
        <source>Uploading failed.</source>
        <translation>Hochladen fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="306"/>
        <source>Unknown source type: %0</source>
        <translation>Unbekannte Quellentyp: %0</translation>
    </message>
</context>
<context>
    <name>ConfigBuild</name>
    <message>
        <location filename="../build/ui_ConfigBuild.h" line="56"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigBuild.h" line="57"/>
        <source>Filter devices list</source>
        <translation>Filtern der Schnittstellen</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigBuild.h" line="58"/>
        <source>Verbose output</source>
        <translation>Detaillierte Ausgabe</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="267"/>
        <source>Configuration</source>
        <translation>Konfiguration</translation>
    </message>
</context>
<context>
    <name>ConfigEditor</name>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="200"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="201"/>
        <source>Font:</source>
        <translation>Schrift:</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="203"/>
        <source>Choose</source>
        <translation>Wähle</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="204"/>
        <source>Color:</source>
        <translation>Farbe:</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="205"/>
        <source>Selection</source>
        <translation>Auswahl</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="207"/>
        <source>Cursor</source>
        <translation>Mauszeiger</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="209"/>
        <source>Foreground</source>
        <translation>Vordergrund</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="211"/>
        <source>Background</source>
        <translation>Hintergrund</translation>
    </message>
</context>
<context>
    <name>ConfigPaths</name>
    <message>
        <location filename="../build/ui_ConfigPaths.h" line="95"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigPaths.h" line="96"/>
        <source>Arduino path:</source>
        <translation>Verzeichniss von Arduino:</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigPaths.h" line="97"/>
        <location filename="../build/ui_ConfigPaths.h" line="99"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigPaths.h" line="98"/>
        <source>Sketchbook path:</source>
        <translation>Verzeichniss von Sketchbook:</translation>
    </message>
</context>
<context>
    <name>ConfigWidget</name>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="94"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="95"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="96"/>
        <source>CommentLine</source>
        <translation>Kommentarlinie</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="97"/>
        <source>CommentDoc</source>
        <translation>Kommentar Documentation</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="98"/>
        <source>Number</source>
        <translation>Nummer</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="99"/>
        <source>Keyword</source>
        <translation>Schlüsselwort</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="100"/>
        <source>DoubleQuotedString</source>
        <translation>Doppelte Anführungszeichen</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="101"/>
        <source>SingleQuotedString</source>
        <translation>Einfache Anführungszeichen</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="102"/>
        <source>UUID</source>
        <translation>UUID</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="103"/>
        <source>PreProcessor</source>
        <translation>PreProzessor</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="104"/>
        <source>Operator</source>
        <translation>Operator</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="105"/>
        <source>Identifier</source>
        <translation>Bezeichner</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="106"/>
        <source>UnclosedString</source>
        <translation>Fehlende Anführungszeichen</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="107"/>
        <source>VerbatimString</source>
        <translation>Wortwörtlich</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="108"/>
        <source>Regex</source>
        <translation>Regex</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="109"/>
        <source>CommentLineDoc</source>
        <translation>CommentLineDoc</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="110"/>
        <source>KeywordSet2</source>
        <translation>Schlüsselsatz 2</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="111"/>
        <source>CommentDocKeyword</source>
        <translation>CommentDocKeyword</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="112"/>
        <source>CommentDocKeywordError</source>
        <translation>CommentDocKeywordError</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="113"/>
        <source>GlobalClass</source>
        <translation>Globale Klasse</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="114"/>
        <source>Editor</source>
        <translation>Bearbeiter</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="118"/>
        <source>Paths</source>
        <translation>Verzeichniss</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="122"/>
        <source>Build</source>
        <translation>Kompilliere</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="206"/>
        <source>Choose Arduino path</source>
        <translation>Wähle Verzeichniss des SDK Arduino</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="213"/>
        <source>Choose Sketchbook path</source>
        <translation>Wähle Verzeichnis für das Sketchbook</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="235"/>
        <source>Invalid arduino path</source>
        <translation>Verzeichniss SDK Arduino existiert nicht</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="235"/>
        <source>This path does not contain a valid Arduino installation, please choose another.</source>
        <translation>In diesem Verzeichniss existiert keine komplette Arduino Installation.</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="239"/>
        <source>Please restart the ArduIDE</source>
        <translation>Bitte arduino-IDE neu starten.</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="239"/>
        <source>Please restart the ArduIDE to account for the Arduino SDK path change.</source>
        <translation>Bitte arduino-IDE neu starten, um die Änderungen des SDK Arduino zu übernehmen.</translation>
    </message>
</context>
<context>
    <name>DeviceChooser</name>
    <message>
        <location filename="../gui/DeviceChooser.cpp" line="16"/>
        <source>Device</source>
        <translation>Schnittstelle</translation>
    </message>
</context>
<context>
    <name>Editor</name>
    <message>
        <location filename="../gui/Editor.cpp" line="30"/>
        <source>Save project</source>
        <translation>Projekt speichern</translation>
    </message>
    <message>
        <location filename="../gui/Editor.cpp" line="30"/>
        <source>Directories (*)</source>
        <translation>Verzeichnisse (*)</translation>
    </message>
    <message>
        <location filename="../gui/Editor.cpp" line="36"/>
        <location filename="../gui/Editor.cpp" line="46"/>
        <source>Save error</source>
        <translation>Fehler beim Speichern</translation>
    </message>
    <message>
        <location filename="../gui/Editor.cpp" line="36"/>
        <source>The specified location could not be opened for writing.</source>
        <translation>Das gewählte Verzeichniss kann nicht beschrieben werden.</translation>
    </message>
    <message>
        <location filename="../gui/Editor.cpp" line="46"/>
        <source>The file could not be opened for writing.</source>
        <translation>Datei konnte nicht geöffnet werden zum schreiben.</translation>
    </message>
</context>
<context>
    <name>FirstTimeWizard</name>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="310"/>
        <source>Wizard</source>
        <translation>Assistent</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="311"/>
        <source>First-time configuration</source>
        <translation>Erstmalige Konfiguration</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="312"/>
        <source>The Arduino IDE needs some information in order to work correctly.&lt;br /&gt;Please fill them in the form below:</source>
        <translation>Die Arduino IDE benötigt einige Informationen um zu funktionieren&lt;br /&gt;Bitte das Formular unten ausfüllen :</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="313"/>
        <source>I need to locate your Arduino installation to continue.</source>
        <translation>Ich brauche das Verzeichniss der Arduino Installation.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="314"/>
        <source>Existing installation (Arduino SDK 0023, 1.0 or 1.0.1)</source>
        <translation>Existierende Installation (Arduino SDK 0023, 1.0 ou 1.0.1)</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="315"/>
        <source>Arduino path:</source>
        <translation>Verzeichniss :</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="316"/>
        <location filename="../build/ui_FirstTimeWizard.h" line="322"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="317"/>
        <source>Automatic installation (Arduino SDK 1.0.1)</source>
        <translation>Automatische Installation (Arduino SDK 1.0.1)</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="318"/>
        <source>&lt;img src=&quot;:/images/16x16/help-hint.png&quot; /&gt;This will download and install the software automatically from &lt;a href=&quot;http://arduino.cc/&quot;&gt;the Arduino website&lt;/a&gt;.</source>
        <translation>&lt;img src=&quot;:/images/16x16/help-hint.png&quot; /&gt;Diese Option lädt und installiert Arduino automatisch von &lt;a href=&quot;http://arduino.cc/&quot;&gt;dWebseite von Arduino&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="319"/>
        <location filename="../build/ui_FirstTimeWizard.h" line="328"/>
        <source>&lt;hr /&gt;</source>
        <translation>&lt;hr /&gt;</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="320"/>
        <source>The sketchbook folder is where the IDE will look for existing projects.&lt;br /&gt;It will be created if it does not already exist.</source>
        <translation>Das Verzeichniss sketchbook ist der Ort wo die IDE nach existierenden Projekten sucht, falls es nicht existiert, wird es erstellt.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="321"/>
        <source>Sketchbook path:</source>
        <translation>Verzeichniss Sketchbook :</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="323"/>
        <source>Automatic Installation</source>
        <translation>Automatische Installation</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="324"/>
        <source>The Arduino IDE will now download and install the required software.</source>
        <translation>Arduino IDE wird nun die nötigen Datein runterladen und installieren.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="325"/>
        <source>Step 1: Download</source>
        <translation>Schritt 1: Herunterladen</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="326"/>
        <location filename="../build/ui_FirstTimeWizard.h" line="330"/>
        <source>TextLabel</source>
        <translation>Texte</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="327"/>
        <source>Arduino %0 for %1</source>
        <translation>Arduino %0 für %1</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="329"/>
        <source>Step 2: Installation</source>
        <translation>Schritt 2: Installation</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="331"/>
        <source>Configuration successful</source>
        <translation>Konfiguration erfolgreich</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="332"/>
        <source>The Arduino IDE is now ready.</source>
        <translation>Die Arduino IDE ist nun bereit.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="333"/>
        <source>The configuration is complete, you may now start using the IDE.&lt;br /&gt;Thank you for using the software. Enjoy!</source>
        <translation>Die Konfiguration ist beendet, die IDE kann nun benutzt werden.&lt;br /&gt; Danke dass Sie diese Software benutzen. Enjoy!</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="334"/>
        <source>%0 is an unofficial project by %1.</source>
        <translation>%0 ist kein offizielles Projekt von %1.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="335"/>
        <source>More information at: &lt;a href=&quot;%0&quot;&gt;%0&lt;/a&gt;</source>
        <translation>Mehr Information auf: &lt;a href=&quot;%0&quot;&gt;%0&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="139"/>
        <source>Applications (*.app)</source>
        <translation>Anwendung (*.app)</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="183"/>
        <location filename="../gui/FirstTimeWizard.cpp" line="194"/>
        <source>Invalid path</source>
        <translation>Fehlerhaftes Verzeichniss</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="183"/>
        <source>Please enter a valid path to your Arduino installation.</source>
        <translation>Bitte ein gültiges Verzeichniss für die Arduino Installation angeben.</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="194"/>
        <source>The sketchbook path could not be created. Please choose another.</source>
        <translation>Das Verzeichniss sketchbook konnte nicht erstellt werden, Bitte ein anderes Verzeichniss wählen.</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="221"/>
        <source>Download error</source>
        <translation>Fehler beim Runterladen</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="222"/>
        <source>An error occured during the download:</source>
        <translation>Ein Fehler ist aufgetreten währen dem runterladen:</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="282"/>
        <location filename="../gui/FirstTimeWizard.cpp" line="296"/>
        <source>Installation error</source>
        <translation>Installationsfehler</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="283"/>
        <source>An error occured during the extraction.</source>
        <translation>Fehler beim Entpacken von Arduino.</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="297"/>
        <source>The extracted Arduino package is not valid.</source>
        <translation>Die ausgepackt Arduinodatei ist fehlerhaft.</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="306"/>
        <source>Installation</source>
        <translation>Installation</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="307"/>
        <source>Arduino was successfully installed to:</source>
        <translation>Arduino wurd erfolgreich installiert in:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../build/ui_MainWindow.h" line="450"/>
        <source>&amp;Quit</source>
        <translation>&amp;Verlassen</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="451"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="452"/>
        <source>&amp;New</source>
        <translation>&amp;Neu</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="453"/>
        <source>Ctrl+N</source>
        <translation>Ctrl-N</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="454"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopieren</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="455"/>
        <source>Ctrl+C</source>
        <translation>Ctrl-C</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="456"/>
        <source>&amp;Paste</source>
        <translation>&amp;Einfügen</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="457"/>
        <source>Ctrl+V</source>
        <translation>Ctrl-V</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="458"/>
        <source>&amp;Cut</source>
        <translation>&amp;Ausschneiden</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="459"/>
        <source>Ctrl+X</source>
        <translation>Ctrl-X</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="460"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="461"/>
        <source>Ctrl+S</source>
        <translation>Ctrl-S</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="462"/>
        <source>&amp;Open</source>
        <translation>&amp;Öffnen</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="463"/>
        <source>Ctrl+O</source>
        <translation>Ctrl-O</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="464"/>
        <source>&amp;Close</source>
        <translation>&amp;Schliessen</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="465"/>
        <source>Ctrl+W</source>
        <translation>Ctrl-W</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="466"/>
        <source>&amp;Build (verify)</source>
        <translation>&amp;Erstellen (Prüfen)</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="467"/>
        <source>Ctrl+R</source>
        <translation>Ctrl-R</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="468"/>
        <source>&amp;Utilities</source>
        <translation>&amp;Werkzeuge</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="470"/>
        <source>Utilities</source>
        <translation>Werkzeuge</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="472"/>
        <source>Ctrl+G</source>
        <translation>Ctrl-G</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="473"/>
        <source>&amp;Upload</source>
        <translation>&amp;Hochladen</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="474"/>
        <source>Ctrl+U</source>
        <translation>Ctrl-U</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="475"/>
        <source>Go to the next tab</source>
        <translation>nächster Reiter</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="476"/>
        <source>Ctrl+PgDown</source>
        <translation>Ctrl+PgDown</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="477"/>
        <source>Go to the previous tab</source>
        <translation>vorheriger Reiter</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="478"/>
        <source>Ctrl+PgUp</source>
        <translation>Ctrl+PgUp</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="479"/>
        <source>&amp;Configure the IDE</source>
        <translation>&amp;Konfiguriere die IDE</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="480"/>
        <source>&amp;About %0</source>
        <translation>&amp;Über %0</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="481"/>
        <source>About &amp;Qt</source>
        <translation>Über &amp;Qt</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="482"/>
        <source>Undo</source>
        <translation>Rückgängig</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="483"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl-Z</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="484"/>
        <source>Redo</source>
        <translation>Nochmals</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="485"/>
        <source>Ctrl+Shift+Z</source>
        <translation>Ctrl-Shift-Z</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="486"/>
        <location filename="../build/ui_MainWindow.h" line="488"/>
        <source>Previous</source>
        <translation>Vorheriger</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="490"/>
        <location filename="../build/ui_MainWindow.h" line="492"/>
        <source>Next</source>
        <translation>Nachfolger</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="494"/>
        <source>Contextual help</source>
        <translation>Kontext Hilfe</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="495"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="496"/>
        <source>The official arduino website</source>
        <translation>Offizielle Seite von Arduino</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="497"/>
        <source>The arduino forums</source>
        <translation>Andruino Forum</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="498"/>
        <source>Upload to pastebin</source>
        <translation>An pastebin senden</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="499"/>
        <location filename="../build/ui_MainWindow.h" line="514"/>
        <source>Find/Replace</source>
        <translation>Suchen/Ersetzen</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="501"/>
        <source>Refresh</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="508"/>
        <source>Libraries</source>
        <translation>Bibliothek</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="515"/>
        <location filename="../build/ui_MainWindow.h" line="517"/>
        <source>Find</source>
        <translation>Finden</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="500"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="502"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="503"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="504"/>
        <source>&amp;Settings</source>
        <translation>&amp;Einstellungen</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="505"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="506"/>
        <source>Community</source>
        <translation>Allgemeinheit</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="507"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="509"/>
        <source>Edition</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="510"/>
        <source>Hardware</source>
        <translation>Hardware</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="511"/>
        <source>Build tools</source>
        <translation>Werkzeuge</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="512"/>
        <source>Utility dock</source>
        <translation>Dienstprogramme</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="513"/>
        <source>Output</source>
        <translation>Ausgabe</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="516"/>
        <location filename="../build/ui_MainWindow.h" line="518"/>
        <source>Replace</source>
        <translation>Ersetzen</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="519"/>
        <source>Replace All</source>
        <translation>Alle ersetzen</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="520"/>
        <source>Reg. Exp.</source>
        <translation>Regex</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="521"/>
        <source>Word only</source>
        <translation>ganzes Wort</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="522"/>
        <source>Case sensitive</source>
        <translation>Gross/Kleinschreibung</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="135"/>
        <source>Browser</source>
        <translation>Browser</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="164"/>
        <source>Device</source>
        <translation>Schnittstelle</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="181"/>
        <source>Board</source>
        <translation>Print</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="247"/>
        <source>Close project</source>
        <translation>Projekt schliessen</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="248"/>
        <source>This project has unsaved changes.&lt;br /&gt;Are you sure you want to close it?</source>
        <translation>Nicht alle Änderungen sind gespeichert.&lt;br /&gt;Soll trotzdem geschlossen werden?</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="360"/>
        <location filename="../gui/MainWindow.cpp" line="380"/>
        <source>Arduino Libraries</source>
        <translation>Arduino Bibliothek</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="370"/>
        <source>ArduIDE Libraries</source>
        <translation>ArduIDE Bibliothek</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="389"/>
        <source>Install new libraries?</source>
        <translation>Installieren neue Bibliothek ?</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="426"/>
        <source>Pastebin error</source>
        <translation>pastebin Fehler</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="426"/>
        <source>The pastebin upload failed with code:
%1</source>
        <translation>Hochladen in pastebin endete mit dem Fehler:
%1</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="466"/>
        <source>Arduide - No occurence found</source>
        <translation>Arduide - kein vorkommen</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="467"/>
        <source>No occurence of &apos;%1&apos; found</source>
        <translation>Kein vorkommen von &apos;%1&apos; gefunden</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="503"/>
        <source>Arduide - Replace All</source>
        <translation>Arduide - Alle ersetzen</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="504"/>
        <source>The Replace All feature replaced %1 occurences</source>
        <translation>Es wurden %1 Vorkommen ersetzt</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="540"/>
        <source>To install a new library, just extract it in this directory.

More information at http://arduino.cc/en/Guide/Environment#libraries</source>
        <translation>Um eine neue Bibliothek zu installieren, Datei in dieses Verzeichniss entpacken.

Mehr Informationen findet man auf http://arduino.cc/en/Guide/Environment#libraries</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="554"/>
        <source>Open project</source>
        <translation>Projekt öffnen</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="554"/>
        <source>Arduino sketches (*.ino *.pde)</source>
        <translation>Arduino sketches (*.ino *.pde)</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="562"/>
        <source>Open error</source>
        <translation>Fehler beim öffnen</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="562"/>
        <source>The file could not be opened for reading.</source>
        <translation>Die Datei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="677"/>
        <source>Quit</source>
        <translation>Verlassen</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="678"/>
        <source>Some projects in your workspace have unsaved changes.&lt;br /&gt;Are you sure you want to quit?</source>
        <translation>Einige Projekte haben noch ungesicherte Änderungen.&lt;br /&gt;Trotzdem verlassen?</translation>
    </message>
</context>
<context>
    <name>OutputView</name>
    <message>
        <location filename="../gui/OutputView.cpp" line="45"/>
        <source>&gt;&gt;&gt;&gt; %0</source>
        <translation>&gt;&gt;&gt;&gt; %0</translation>
    </message>
</context>
<context>
    <name>QHexView</name>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="144"/>
        <source>Set &amp;Font</source>
        <translation>Schrift &amp; wählen</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="146"/>
        <source>Show A&amp;ddress</source>
        <translation>Zeige eine &apos;A&amp;dresse</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="147"/>
        <source>Show &amp;Hex</source>
        <translation>Zeige &amp;Hex</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="148"/>
        <source>Show &amp;Ascii</source>
        <translation>Zeige &amp;Ascii</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="153"/>
        <source>Set Word Width</source>
        <translation>Wortlänge setzen</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="154"/>
        <source>1 Byte</source>
        <translation>1 Byte</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="155"/>
        <source>2 Bytes</source>
        <translation>2 Bytes</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="156"/>
        <source>4 Bytes</source>
        <translation>4 Bytes</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="157"/>
        <source>8 Bytes</source>
        <translation>8 Bytes</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="168"/>
        <source>Set Row Width</source>
        <translation>Setze Linienbreite</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="169"/>
        <source>1 Word</source>
        <translation>1 Word</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="170"/>
        <source>2 Words</source>
        <translation>2 Word</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="171"/>
        <source>4 Words</source>
        <translation>4 Word</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="172"/>
        <source>8 Words</source>
        <translation>8 Word</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="173"/>
        <source>16 Words</source>
        <translation>16 Word</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="188"/>
        <source>&amp;Copy Selection To Clipboard</source>
        <translation>&amp;Auswahl nach Zwischenablage</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../env/Device.cpp" line="97"/>
        <source>Serial callout</source>
        <translation>Serial callout</translation>
    </message>
    <message>
        <location filename="../env/Device.cpp" line="111"/>
        <source>Serial dialin</source>
        <translation>Serial dialin</translation>
    </message>
</context>
<context>
    <name>Serial</name>
    <message>
        <location filename="../utils/unix/Serial.cpp" line="23"/>
        <source>Device (%0) already open</source>
        <translation>Gerät (%0) ist schon offen</translation>
    </message>
    <message>
        <location filename="../utils/unix/Serial.cpp" line="40"/>
        <source>Unknown baud rate %0</source>
        <translation>Unbekannte Baudrate %0</translation>
    </message>
</context>
</TS>
