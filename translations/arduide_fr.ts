<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="117"/>
        <source>About %0</source>
        <translation>À Propos %0</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="118"/>
        <source>%0</source>
        <translation>%0</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="119"/>
        <source>&lt;a href=&quot;%0&quot;&gt;%0&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;%0&quot;&gt;%0&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="120"/>
        <source>Authors: %0</source>
        <translation>Auteurs: %0</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="121"/>
        <source>License: %0</source>
        <translation>Licence: %0</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="122"/>
        <source>Version: %0</source>
        <translation>Version: %0</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="123"/>
        <source>Internationalization : &lt;a href=&quot;%0&quot;&gt;You can help us translate on transifex.com&lt;/a&gt;</source>
        <translation>Internationalisation : &lt;a href=&quot;%0&quot;&gt;Vous pouvez nous aider à traduire sur transifex.com&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="124"/>
        <source>&lt;hr /&gt;</source>
        <translation>&lt;hr /&gt;</translation>
    </message>
    <message>
        <location filename="../build/ui_AboutDialog.h" line="125"/>
        <source>&lt;p&gt;%0 is a Qt-based IDE for the open-source &lt;a href=&quot;http://arduino.cc&quot;&gt;Arduino&lt;/a&gt; electronics prototyping platform.&lt;/p&gt;

&lt;div&gt;This project is an attempt to provide an alternative to the original Java IDE.&lt;/div&gt;
&lt;div&gt;It is faster and provides a richer feature set for experienced developers.&lt;/div&gt;</source>
        <translation>&lt;p&gt;%0 est un IDE basé sur Qt pour la plateforme électronique &lt;a href=&quot;http://arduino.cc&quot;&gt;Arduino&lt;/a&gt; &lt;/p&gt;

&lt;div&gt;Ce projet propose une alternative à l&apos;IDE par défaut en JAVA.&lt;/div&gt;
&lt;div&gt;Il est plus rapide et offre un riche ensemble de fonctionnalités pour les développeurs expérimentés.&lt;/div&gt;</translation>
    </message>
</context>
<context>
    <name>BoardChooser</name>
    <message>
        <location filename="../gui/BoardChooser.cpp" line="16"/>
        <source>Board</source>
        <translation>Carte</translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <location filename="../gui/Browser.cpp" line="145"/>
        <location filename="../gui/Browser.cpp" line="159"/>
        <source>Load error</source>
        <translation>Erreur lors du chargement</translation>
    </message>
    <message>
        <location filename="../gui/Browser.cpp" line="145"/>
        <location filename="../gui/Browser.cpp" line="159"/>
        <source>The selected example could not be opened.</source>
        <translation>L&apos;exemple choisi ne peut pas être ouvert.</translation>
    </message>
    <message>
        <location filename="../gui/Browser.cpp" line="277"/>
        <source>No documentation found for %1.</source>
        <translation>Aucune aide trouvée pour %1.</translation>
    </message>
</context>
<context>
    <name>Builder</name>
    <message>
        <location filename="../env/Builder.cpp" line="42"/>
        <source>Cannot read file &apos;%1&apos;.</source>
        <translation>Lecture du fichier &apos;%1&apos; impossible.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="70"/>
        <location filename="../env/Builder.cpp" line="98"/>
        <source>Failed to create build directory.</source>
        <translation>Erreur lors de la création du répertoire de build.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="127"/>
        <source>No board selected.</source>
        <translation>Aucune carte selectionnée.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="133"/>
        <source>No device selected.</source>
        <translation>Aucun port selectionné.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="137"/>
        <source>Compiling for %0...</source>
        <translation>Compilation de %0...</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="161"/>
        <location filename="../env/Builder.cpp" line="183"/>
        <location filename="../env/Builder.cpp" line="217"/>
        <source>Compilation failed.</source>
        <translation>Erreur de compilation.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="168"/>
        <source>Archiving failed.</source>
        <translation>La création de la lib core à échoué.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="192"/>
        <source>Can&apos;t write the sketch to disk.</source>
        <translation>Ecriture du sketch sur disque impossible.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="204"/>
        <source>Can&apos;t open main.cxx.</source>
        <translation>Erreur d&apos;ouverture de main.cxx.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="222"/>
        <source>Linking...</source>
        <translation>Linkage...</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="226"/>
        <source>Link failed.</source>
        <translation>Le linkage à échoué.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="231"/>
        <source>Sizing...</source>
        <translation>Dimensionnement...</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="234"/>
        <source>Sizing failed.</source>
        <translation>Echec du Dimensionnement.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="240"/>
        <source>Failed to extract EEPROM.</source>
        <translation>Erreur lors de l&apos;extraction EEPROM.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="248"/>
        <source>Failed to extract HEX.</source>
        <translation>Erreur lors de l&apos;extraction HEX.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="254"/>
        <location filename="../env/Builder.cpp" line="266"/>
        <source>Success.</source>
        <translation>Succes.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="259"/>
        <source>Uploading to %0...</source>
        <translation>Chargement sur %0...</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="262"/>
        <source>Uploading failed.</source>
        <translation>Echec du chargement.</translation>
    </message>
    <message>
        <location filename="../env/Builder.cpp" line="306"/>
        <source>Unknown source type: %0</source>
        <translation>Source inconnue: %0</translation>
    </message>
</context>
<context>
    <name>ConfigBuild</name>
    <message>
        <location filename="../build/ui_ConfigBuild.h" line="56"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigBuild.h" line="57"/>
        <source>Filter devices list</source>
        <translation>Filtrer la liste des ports</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigBuild.h" line="58"/>
        <source>Verbose output</source>
        <translation>Sortie détaillée</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="267"/>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
</context>
<context>
    <name>ConfigEditor</name>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="200"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="201"/>
        <source>Font:</source>
        <translation>Police:</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="203"/>
        <source>Choose</source>
        <translation>Choisir</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="204"/>
        <source>Color:</source>
        <translation>Couleur:</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="205"/>
        <source>Selection</source>
        <translation>Selection</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="207"/>
        <source>Cursor</source>
        <translation>Curseur</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="209"/>
        <source>Foreground</source>
        <translation>Avant Plan</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigEditor.h" line="211"/>
        <source>Background</source>
        <translation>Arrière Plan</translation>
    </message>
</context>
<context>
    <name>ConfigPaths</name>
    <message>
        <location filename="../build/ui_ConfigPaths.h" line="95"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigPaths.h" line="96"/>
        <source>Arduino path:</source>
        <translation>Chemin vers Arduino:</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigPaths.h" line="97"/>
        <location filename="../build/ui_ConfigPaths.h" line="99"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../build/ui_ConfigPaths.h" line="98"/>
        <source>Sketchbook path:</source>
        <translation>Chemin vers Sketchbook:</translation>
    </message>
</context>
<context>
    <name>ConfigWidget</name>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="94"/>
        <source>Default</source>
        <translation>Normale</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="95"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="96"/>
        <source>CommentLine</source>
        <translation>LigneCommentaire</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="97"/>
        <source>CommentDoc</source>
        <translation>DocCommentaire</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="98"/>
        <source>Number</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="99"/>
        <source>Keyword</source>
        <translation>MotClé</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="100"/>
        <source>DoubleQuotedString</source>
        <translation>ChaineCaractèreGuillemets</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="101"/>
        <source>SingleQuotedString</source>
        <translation>ChaineCaractèreGuillemetsSimple</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="102"/>
        <source>UUID</source>
        <translation>UUID</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="103"/>
        <source>PreProcessor</source>
        <translation>PréProcesseur</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="104"/>
        <source>Operator</source>
        <translation>Opérateur</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="105"/>
        <source>Identifier</source>
        <translation>Identifiant</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="106"/>
        <source>UnclosedString</source>
        <translation>ChaineCaractèreNonFermée</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="107"/>
        <source>VerbatimString</source>
        <translation>ChaineCaractèreVerbatim</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="108"/>
        <source>Regex</source>
        <translation>ExpReg</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="109"/>
        <source>CommentLineDoc</source>
        <translation>DocCommentaireLigne</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="110"/>
        <source>KeywordSet2</source>
        <translation>JeuMotClé2</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="111"/>
        <source>CommentDocKeyword</source>
        <translation>CommentaireDocMotClé</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="112"/>
        <source>CommentDocKeywordError</source>
        <translation>CommentaireDocMotCléErreur</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="113"/>
        <source>GlobalClass</source>
        <translation>ClassGlobale</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="114"/>
        <source>Editor</source>
        <translation>Éditeur</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="118"/>
        <source>Paths</source>
        <translation>Chemins</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="122"/>
        <source>Build</source>
        <translation>Compilation</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="206"/>
        <source>Choose Arduino path</source>
        <translation>Sélectionner le chemin du SDK Arduino</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="213"/>
        <source>Choose Sketchbook path</source>
        <translation>Sélectionner le chemin du Sketchbook</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="235"/>
        <source>Invalid arduino path</source>
        <translation>Chemin du SDK Arduino invalide</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="235"/>
        <source>This path does not contain a valid Arduino installation, please choose another.</source>
        <translation>Ce chemin the contient pas d&apos;installation Arduino valide, veuillez en choisir un autre.</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="239"/>
        <source>Please restart the ArduIDE</source>
        <translation>Veuillez redémarrer l&apos;ArduIDE</translation>
    </message>
    <message>
        <location filename="../gui/ConfigDialog.cpp" line="239"/>
        <source>Please restart the ArduIDE to account for the Arduino SDK path change.</source>
        <translation>Veuillez redémarrer l&apos;ArduIDE pour prendre en compte le changement de SDK Arduino.</translation>
    </message>
</context>
<context>
    <name>DeviceChooser</name>
    <message>
        <location filename="../gui/DeviceChooser.cpp" line="16"/>
        <source>Device</source>
        <translation>Port</translation>
    </message>
</context>
<context>
    <name>Editor</name>
    <message>
        <location filename="../gui/Editor.cpp" line="30"/>
        <source>Save project</source>
        <translation>Sauver le projet</translation>
    </message>
    <message>
        <location filename="../gui/Editor.cpp" line="30"/>
        <source>Directories (*)</source>
        <translation>Répertoires (*)</translation>
    </message>
    <message>
        <location filename="../gui/Editor.cpp" line="36"/>
        <location filename="../gui/Editor.cpp" line="46"/>
        <source>Save error</source>
        <translation>Erreur de sauvegarde</translation>
    </message>
    <message>
        <location filename="../gui/Editor.cpp" line="36"/>
        <source>The specified location could not be opened for writing.</source>
        <translation>Le répertoire ne peut pas être ouvert en écriture.</translation>
    </message>
    <message>
        <location filename="../gui/Editor.cpp" line="46"/>
        <source>The file could not be opened for writing.</source>
        <translation>Le Fichier ne peut pas être ouvert en écriture.</translation>
    </message>
</context>
<context>
    <name>FirstTimeWizard</name>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="310"/>
        <source>Wizard</source>
        <translation>Assistant</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="311"/>
        <source>First-time configuration</source>
        <translation>Première Configuration</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="312"/>
        <source>The Arduino IDE needs some information in order to work correctly.&lt;br /&gt;Please fill them in the form below:</source>
        <translation>L&apos;IDE Arduino a besoin de quelques informations pour fonctionner correctement&lt;br /&gt;Merci de les renseigner ci dessous :</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="313"/>
        <source>I need to locate your Arduino installation to continue.</source>
        <translation>J&apos;ai besoin de localiser votre installation Arduino pour pouvoir continuer.</translation>
    </message>
    <message>
        <source>Existing installation</source>
        <translation type="obsolete">Installation existante</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="315"/>
        <source>Arduino path:</source>
        <translation>Chemin :</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="316"/>
        <location filename="../build/ui_FirstTimeWizard.h" line="322"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <source>Automatic installation</source>
        <translation type="obsolete">Installation automatique</translation>
    </message>
    <message>
        <source>Existing installation (Arduino SDK 0023 or 1.0)</source>
        <translation type="obsolete">Installation existante (Arduino SDK 0023 or 1.0)</translation>
    </message>
    <message>
        <source>Automatic installation (Arduino SDK 1.0)</source>
        <translation type="obsolete">Installation automatique (Arduino SDK 1.0)</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="314"/>
        <source>Existing installation (Arduino SDK 0023, 1.0 or 1.0.1)</source>
        <translation>Installation existante (Arduino SDK 0023, 1.0 ou 1.0.1)</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="317"/>
        <source>Automatic installation (Arduino SDK 1.0.1)</source>
        <translation>Installation automatique (Arduino SDK 1.0.1)</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="318"/>
        <source>&lt;img src=&quot;:/images/16x16/help-hint.png&quot; /&gt;This will download and install the software automatically from &lt;a href=&quot;http://arduino.cc/&quot;&gt;the Arduino website&lt;/a&gt;.</source>
        <translation>&lt;img src=&quot;:/images/16x16/help-hint.png&quot; /&gt;Ceci va télécharger et installer la suite logicielle Arduino depuis &lt;a href=&quot;http://arduino.cc/&quot;&gt;le site web Arduino&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="319"/>
        <location filename="../build/ui_FirstTimeWizard.h" line="328"/>
        <source>&lt;hr /&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="320"/>
        <source>The sketchbook folder is where the IDE will look for existing projects.&lt;br /&gt;It will be created if it does not already exist.</source>
        <translation>Le répertoire sketchbook est l&apos;endroit ou l&apos;IDE va chercher les projets existants.&lt;br /&gt;Il sera crée s&apos;il n&apos;est pas déjà existant.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="321"/>
        <source>Sketchbook path:</source>
        <translation>Chemin :</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="323"/>
        <source>Automatic Installation</source>
        <translation>Installation Automatique</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="324"/>
        <source>The Arduino IDE will now download and install the required software.</source>
        <translation>La suite logicielle Arduino va maintenant être téléchargée puis installée.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="325"/>
        <source>Step 1: Download</source>
        <translation>Phase 1: Téléchargement</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="326"/>
        <location filename="../build/ui_FirstTimeWizard.h" line="330"/>
        <source>TextLabel</source>
        <translation>Texte</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="327"/>
        <source>Arduino %0 for %1</source>
        <translation>Arduino %0 pour %1</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="329"/>
        <source>Step 2: Installation</source>
        <translation>Phase 2: Installation</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="331"/>
        <source>Configuration successful</source>
        <translation>Configuration réussie</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="332"/>
        <source>The Arduino IDE is now ready.</source>
        <translation>L&apos;IDE Arduino est opérationnel.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="333"/>
        <source>The configuration is complete, you may now start using the IDE.&lt;br /&gt;Thank you for using the software. Enjoy!</source>
        <translation>La configuration est achevée, vous pouvez maintenant utiliser l&apos;IDE&lt;br /&gt; Merci d&apos;utiliser cette application. Enjoy!</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="334"/>
        <source>%0 is an unofficial project by %1.</source>
        <translation>%0 n&apos;est pas un projet officiel de %1.</translation>
    </message>
    <message>
        <location filename="../build/ui_FirstTimeWizard.h" line="335"/>
        <source>More information at: &lt;a href=&quot;%0&quot;&gt;%0&lt;/a&gt;</source>
        <translation>Plus d&apos;informations à: &lt;a href=&quot;%0&quot;&gt;%0&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="139"/>
        <source>Applications (*.app)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="183"/>
        <location filename="../gui/FirstTimeWizard.cpp" line="194"/>
        <source>Invalid path</source>
        <translation>Chemin Invalide</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="183"/>
        <source>Please enter a valid path to your Arduino installation.</source>
        <translation>Merci de renseigner un chemin valide vers votre installation de la suite logicielle Arduino.</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="194"/>
        <source>The sketchbook path could not be created. Please choose another.</source>
        <translation>Le répertoire sketchbook ne peut pas être crée. Merci d&apos;en choisir un autre.</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="221"/>
        <source>Download error</source>
        <translation>Erreur lors du téléchargement</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="222"/>
        <source>An error occured during the download:</source>
        <translation>Une erreur s&apos;est produit lors du téléchargement:</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="282"/>
        <location filename="../gui/FirstTimeWizard.cpp" line="296"/>
        <source>Installation error</source>
        <translation>Erreur d&apos;installation</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="283"/>
        <source>An error occured during the extraction.</source>
        <translation>Une erreur s&apos;est produite lors de l&apos;extraction.</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="297"/>
        <source>The extracted Arduino package is not valid.</source>
        <translation>L&apos;archive Arduino n&apos;est pas valide.</translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="306"/>
        <source>Installation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/FirstTimeWizard.cpp" line="307"/>
        <source>Arduino was successfully installed to:</source>
        <translation>La suite Arduino est correctement installé dans :</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Arduino IDE</source>
        <translation type="obsolete">IDE Arduino</translation>
    </message>
    <message>
        <source>Search and Replace</source>
        <translation type="obsolete">Rechercher/Remplacer</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="500"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="502"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="503"/>
        <source>&amp;Edit</source>
        <translation>&amp;Edition</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="504"/>
        <source>&amp;Settings</source>
        <translation>&amp;Configuration</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="505"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="506"/>
        <source>Community</source>
        <translation>Communauté</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="507"/>
        <source>&amp;View</source>
        <translation>&amp;Vue</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="509"/>
        <source>Edition</source>
        <translation>Edition</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="510"/>
        <source>Hardware</source>
        <translation>Hardware</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="511"/>
        <source>Build tools</source>
        <translation>Outils de Construction</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="512"/>
        <source>Utility dock</source>
        <translation>Utility dock</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="513"/>
        <source>Output</source>
        <translation>Sortie</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="450"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="451"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="452"/>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="453"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="454"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copier</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="455"/>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="456"/>
        <source>&amp;Paste</source>
        <translation>&amp;Coller</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="457"/>
        <source>Ctrl+V</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="458"/>
        <source>&amp;Cut</source>
        <translation>&amp;Couper</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="459"/>
        <source>Ctrl+X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="460"/>
        <source>&amp;Save</source>
        <translation>&amp;Sauver</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="461"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="462"/>
        <source>&amp;Open</source>
        <translation>&amp;Ouvrir</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="463"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="464"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="465"/>
        <source>Ctrl+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="466"/>
        <source>&amp;Build (verify)</source>
        <translation>&amp;Construire(Verifier)</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="467"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="468"/>
        <source>&amp;Utilities</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="470"/>
        <source>Utilities</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="472"/>
        <source>Ctrl+G</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="473"/>
        <source>&amp;Upload</source>
        <translation>&amp;Envoyer</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="474"/>
        <source>Ctrl+U</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="475"/>
        <source>Go to the next tab</source>
        <translation>Onglet suivant</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="476"/>
        <source>Ctrl+PgDown</source>
        <translation>Ctrl+PgDown</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="477"/>
        <source>Go to the previous tab</source>
        <translation>Onglet Précédent</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="478"/>
        <source>Ctrl+PgUp</source>
        <translation>Ctrl+PgUp</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="479"/>
        <source>&amp;Configure the IDE</source>
        <translation>&amp;Configurer l&apos;IDE</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="480"/>
        <source>&amp;About %0</source>
        <translation>&amp;A Propos %0</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="481"/>
        <source>About &amp;Qt</source>
        <translation>A Propos de &amp;Qt</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="482"/>
        <source>Undo</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="483"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="484"/>
        <source>Redo</source>
        <translation>Refaire</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="485"/>
        <source>Ctrl+Shift+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="486"/>
        <location filename="../build/ui_MainWindow.h" line="488"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="490"/>
        <location filename="../build/ui_MainWindow.h" line="492"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="494"/>
        <source>Contextual help</source>
        <translation>Aide Contextuelle</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="495"/>
        <source>F1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="496"/>
        <source>The official arduino website</source>
        <translation>Site Officiel Arduino</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="497"/>
        <source>The arduino forums</source>
        <translation>Forum Arduino</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="498"/>
        <source>Upload to pastebin</source>
        <translation>Envoyer sur pastebin</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="499"/>
        <location filename="../build/ui_MainWindow.h" line="514"/>
        <source>Find/Replace</source>
        <translation>Rechercher/Remplacer</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="501"/>
        <source>Refresh</source>
        <translation>Rafraîchir</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="508"/>
        <source>Libraries</source>
        <translation>Bibliothèques</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="515"/>
        <location filename="../build/ui_MainWindow.h" line="517"/>
        <source>Find</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Chercher</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="516"/>
        <location filename="../build/ui_MainWindow.h" line="518"/>
        <source>Replace</source>
        <translation>Remplacer</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="519"/>
        <source>Replace All</source>
        <translation>Tout remplacer</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="520"/>
        <source>Reg. Exp.</source>
        <translation>Exp. Reg.</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="521"/>
        <source>Word only</source>
        <translation>Mot seulement</translation>
    </message>
    <message>
        <location filename="../build/ui_MainWindow.h" line="522"/>
        <source>Case sensitive</source>
        <translation>Sensible à la casse</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="135"/>
        <source>Browser</source>
        <translation>Browser</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="164"/>
        <source>Device</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="181"/>
        <source>Board</source>
        <translation>Carte</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="247"/>
        <source>Close project</source>
        <translation>Fermer le projet</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="248"/>
        <source>This project has unsaved changes.&lt;br /&gt;Are you sure you want to close it?</source>
        <translation>Le projet a des modifications non sauvegardées&lt;br /&gt;Etes vous sûr de vouloir fermer ?</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="360"/>
        <location filename="../gui/MainWindow.cpp" line="380"/>
        <source>Arduino Libraries</source>
        <translation>Bibliothèques Arduino</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="370"/>
        <source>ArduIDE Libraries</source>
        <translation>Bibliothèques ArduIDE</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="389"/>
        <source>Install new libraries?</source>
        <translation>Installation de nouvelles bibliothèques ?</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="426"/>
        <source>Pastebin error</source>
        <translation>Erreur pastebin</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="426"/>
        <source>The pastebin upload failed with code:
%1</source>
        <translation>L&apos;envoi sur pastebin à échoué avec le code:
%1</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="466"/>
        <source>Arduide - No occurence found</source>
        <translation>Arduide - Aucune occurence</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="467"/>
        <source>No occurence of &apos;%1&apos; found</source>
        <translation>Aucune occurence de &apos;%1&apos; trouvée</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="503"/>
        <source>Arduide - Replace All</source>
        <translation>Arduide - Tout remplacer</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="504"/>
        <source>The Replace All feature replaced %1 occurences</source>
        <translation>La fonction de remplacement à remplacé %1 occurences</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="540"/>
        <source>To install a new library, just extract it in this directory.

More information at http://arduino.cc/en/Guide/Environment#libraries</source>
        <translation>Pour installer une nouvelle bibliothèque, veuillez l&apos;extraire dans ce répertoire.

Plus d&apos;informations à l&apos;adresse http://arduino.cc/en/Guide/Environment#libraries</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="554"/>
        <source>Open project</source>
        <translation>Ouvrir projet</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="554"/>
        <source>Arduino sketches (*.ino *.pde)</source>
        <translation>Sketches Arduino (*.ino *.pde)</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="562"/>
        <source>Open error</source>
        <translation>Erreur d&apos;ouverture</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="562"/>
        <source>The file could not be opened for reading.</source>
        <translation>Le fichier ne peut pas être ouvert.</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="677"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../gui/MainWindow.cpp" line="678"/>
        <source>Some projects in your workspace have unsaved changes.&lt;br /&gt;Are you sure you want to quit?</source>
        <translation>Certains projets ont des changements non sauvegardés.&lt;br /&gt;Etes vous sûr de vouloir quitter ?</translation>
    </message>
</context>
<context>
    <name>OutputView</name>
    <message>
        <location filename="../gui/OutputView.cpp" line="45"/>
        <source>&gt;&gt;&gt;&gt; %0</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QHexView</name>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="144"/>
        <source>Set &amp;Font</source>
        <translation>Choisir la &amp;Police</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="146"/>
        <source>Show A&amp;ddress</source>
        <translation>Montrer l&apos;A&amp;dresse</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="147"/>
        <source>Show &amp;Hex</source>
        <translation>Montrer l&apos;&amp;Hexa</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="148"/>
        <source>Show &amp;Ascii</source>
        <translation>Montre l&apos;&amp;Ascii</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="153"/>
        <source>Set Word Width</source>
        <translation>Sélectionner la largeur des mots</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="154"/>
        <source>1 Byte</source>
        <translation>1 Octet</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="155"/>
        <source>2 Bytes</source>
        <translation>2 Octets</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="156"/>
        <source>4 Bytes</source>
        <translation>4 Octets</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="157"/>
        <source>8 Bytes</source>
        <translation>8 Octets</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="168"/>
        <source>Set Row Width</source>
        <translation>Choix de la largeur de ligne</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="169"/>
        <source>1 Word</source>
        <translation>1 Mot</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="170"/>
        <source>2 Words</source>
        <translation>2 Mots</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="171"/>
        <source>4 Words</source>
        <translation>4 Mots</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="172"/>
        <source>8 Words</source>
        <translation>8 Mots</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="173"/>
        <source>16 Words</source>
        <translation>16 Mots</translation>
    </message>
    <message>
        <location filename="../utils/hexview/QHexView.cpp" line="188"/>
        <source>&amp;Copy Selection To Clipboard</source>
        <translation>&amp;Copier la sélection vers le presse papier</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../env/Device.cpp" line="97"/>
        <source>Serial callout</source>
        <translation>Serial callout</translation>
    </message>
    <message>
        <location filename="../env/Device.cpp" line="111"/>
        <source>Serial dialin</source>
        <translation>Serial dialin</translation>
    </message>
</context>
<context>
    <name>Serial</name>
    <message>
        <location filename="../utils/unix/Serial.cpp" line="23"/>
        <source>Device (%0) already open</source>
        <translation>Le périphérique (%0) est déjà ouvert</translation>
    </message>
    <message>
        <location filename="../utils/unix/Serial.cpp" line="40"/>
        <source>Unknown baud rate %0</source>
        <translation>Débit de transmission inconnu %0</translation>
    </message>
</context>
</TS>
